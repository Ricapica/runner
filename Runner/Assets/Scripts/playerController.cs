﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {
    //Game Controller reference
    GameControllerScript gcs;
    Animator animator;

    //Stats
    private float moveSpeed=0;          //Movement speed left and right.
    private float jumpPower = 0;        //Jump Power affects the height jumped.
    private int airJumps = 0;           //Possible consecutive jumps in the air.
    private int airDashes = 0;          //Possible conseutive air dashes.
    private float wallSlideSpeed = 0;   //When latched to a wall, the speed at which we drop down.
    private float damageInvincibilityDuration = 1f;     //Time the character is invincible after taking damage.
    private int maxHealth = 10;         //Highest health the character can have.
    private int currentHealth;          //Current health the player has.
    private float dashSpeed=0;          //Movement speed while dashing.
    private float dashTime = 0f;        //Time to complete a dash.
    private float flinchSpeed = 0f;     //Speed the player is knocked back when takign damage.
    private float flinchTime = 0f;      //Time the player is flinching and can't move.
    private float fireSpeed = 0.5f;     //Time Between bullets shot.
    private float fireTimeLeft = 0f;    //Time until next shot is available.
    private string weaponName = "Bullet_A";     //The name of the weapon currently equiped.
    private float maxFallSpeed = 20f;   //Maximum velocity under gravity.

    //Action States
    bool facingRight = true;            //Which direction the character is facing. False = left.
    bool canRight = true;                 //Can the character move right.
    bool canLeft = true;                  //Can the character move left.
    bool nearWall = false;              //If the character is near a wall from either side.
    bool grabbedWall = false;           //If the character is holding on to a wall.
    bool onJumpingGround = false;         //If the character is standing on ground he can jump from.
   
    

    //Status
    float movementLock = 0;             //Locked will disable player input left and right.
    float wallGrabLock = 0;             //Locked will disable the ability to grab walls.
    float invincibleTime = 0;           //Locked will keep the player invulnerable.
    float dashTimeLeft = 0;             //Dash duration.
    float flinchTimeLeft = 0;           //Flinch duration.

    //Area Movement
    bool onDoorway = false;
    string nextArea = "";
    int doorNum = 0;

    //Animator Variables
    bool landed = false;

    GameObject lastWall;
    GameObject lastGround;

    Material playerMaterial;

    private SpriteRenderer SR;

    private void Awake()
    {
        gcs = GameControllerScript.gcs;
        gcs.player = this.gameObject;
    }

    void Start () {
        
        RequestStats();
        playerMaterial = GetComponent<Renderer>().material;
        SR = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if(dashTimeLeft >= 0||grabbedWall)
        {
           this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        }
        else
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
        if(grabbedWall)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, -wallSlideSpeed, 0);
        }
        
    }

    // Update is called once per frame
    void Update() {

        //Tick down all statuses, uses deltaTime
        ReduceStatus();

        if (Input.GetKey("up") && onDoorway)
        {
            gcs.EnterDoor(nextArea,doorNum);
        }

        

        //Current velocity vector3 of player
        Vector3 currentVelocity = this.GetComponent<Rigidbody>().velocity;
        if (currentVelocity.y < -maxFallSpeed)
        {
            this.gameObject.GetComponent<Rigidbody>().velocity=new Vector3(this.gameObject.GetComponent<Rigidbody>().velocity.x, -maxFallSpeed,0);
        }
        //Dashing
        if (flinchTimeLeft >= 0)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(facingRight ? -flinchSpeed : flinchSpeed, 2, 0);
            
        }
        

        if (invincibleTime>=0)
        {
            //Flicker white
            if ((int)(Time.realtimeSinceStartup * 50) % 2 == 0)
            {
                playerMaterial.SetFloat("_FlashAmount", 1);
            }
            else
            {
                playerMaterial.SetFloat("_FlashAmount", 0);
            }


        }
        else
        {
            playerMaterial.SetFloat("_FlashAmount", 0);
        }

        //Dashing
        if (dashTimeLeft>=0 && gcs.SKILL_DASH)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(facingRight?dashSpeed:-dashSpeed, 0, 0);
        }
        
        //movementlock prevents player input
        if (movementLock <= 0 && gcs.SKILL_MOVE && flinchTimeLeft <= 0)
        {
            if (Input.GetKey("right"))
            {

                //Direction the character is looking
                facingRight = true;

                if (canRight)
                {

                    this.GetComponent<Rigidbody>().velocity = new Vector3(moveSpeed, currentVelocity.y, 0);
                    canLeft = true;
                    grabbedWall = false;
                }
                else if (!onJumpingGround && wallGrabLock <= 0 && gcs.SKILL_WALL_JUMP)
                {
                    //grabbedWall = true;
                    facingRight = false;
                    ResetAirJumps();
                    ResetAirDashes();
                }
            }
            else if (Input.GetKey("left"))
            {
                
                //Direction the character is looking
                facingRight = false;

                if (canLeft)
                {

                    this.GetComponent<Rigidbody>().velocity = new Vector3(-moveSpeed, currentVelocity.y, 0);
                    canRight = true;
                    grabbedWall = false;
                }
                else if (!onJumpingGround && wallGrabLock <= 0 && gcs.SKILL_WALL_JUMP)
                {
                    //grabbedWall = true;
                    facingRight = true;
                    ResetAirJumps();
                    ResetAirDashes();
                }
            }
            else
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(0, currentVelocity.y, 0);
            }
        }

        if (Input.GetKeyDown("x") && flinchTimeLeft <= 0 && dashTimeLeft <=0)
        {
            //The order of the following represents priority. Player will wall jump instead of double jump when possible
            //Jump
            wallGrabLock = 0.25f;
            if (CanJump())
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(currentVelocity.x, jumpPower, 0);
            }
            //Wall Jump
            else if (CanWallJump())
            {
                grabbedWall = false;
                float xspeed = 0;
                if (!canLeft)
                    xspeed = moveSpeed;
                else
                    xspeed = -moveSpeed;
                this.GetComponent<Rigidbody>().velocity = new Vector3(xspeed, jumpPower, 0);
                movementLock = 0.15f;
            }
            //Double Jump
            else if (CanAirJump())
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(currentVelocity.x, jumpPower, 0);
                airJumps = airJumps - 1;
            }
        }

        if(Input.GetKey("c"))
        {
            //Shoot
            if (CanShoot())
            {
                Fire(weaponName);
            }
        }
        if (Input.GetKeyDown("z"))
        {
            //Dash
            if (CanDash())
            {
                dashTimeLeft = dashTime;
                movementLock = dashTimeLeft;
            }
            else if (CanAirDash())
            {
                dashTimeLeft = dashTime;
                movementLock = dashTimeLeft;
                airDashes -= 1;
            }
        }

        //Change direction
        SR.flipX = !facingRight;



        //Animation State Manager
        if (this.gameObject.GetComponent<Rigidbody>().velocity.x >= 0.1 || this.gameObject.GetComponent<Rigidbody>().velocity.x <= -0.1)
        {
            animator.SetBool("isMoving", true);
        }
        else if(this.gameObject.GetComponent<Rigidbody>().velocity.x == 0)
        {
            animator.SetBool("isMoving", false);
        }
        
        animator.SetFloat("y_momentum", this.gameObject.GetComponent<Rigidbody>().velocity.y);

        animator.ResetTrigger("land");
        if (landed)
        {
            animator.SetTrigger("land");
            landed = false;
        }
        else
        {
            animator.ResetTrigger("land");
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        string tag = collision.gameObject.tag;
        if (tag.Equals("Omniwall"))
        {
            bool standCondition = this.gameObject.transform.position.y - this.gameObject.transform.localScale.y / 2 >= collision.gameObject.transform.position.y + collision.gameObject.transform.localScale.y / 2;
            if(standCondition)
            {
                //Once the player stands on the ground, release walls
                onJumpingGround = true;
                landed = true;
                grabbedWall = false;
                ResetAirJumps();
                ResetAirDashes();
                lastGround = collision.gameObject;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        string tag = collision.gameObject.tag;

       
        //OmniWall
        if (tag.Equals("Omniwall"))
        {

            bool standCondition = this.gameObject.transform.position.y - this.gameObject.transform.localScale.y / 2 >= collision.gameObject.transform.position.y + collision.gameObject.transform.localScale.y / 2;

            bool wallCondition = (this.gameObject.transform.position.x - this.gameObject.transform.localScale.x / 2 >= collision.gameObject.transform.position.x + collision.gameObject.transform.localScale.x / 2 + 0.1)
                || (this.gameObject.transform.position.x + this.gameObject.transform.localScale.x / 2 <= collision.gameObject.transform.position.x - collision.gameObject.transform.localScale.x / 2 - 0.1);


            if (standCondition && wallCondition)
            {
                //Caught on the edge of the object, not multiple instances of omniwall
                if(this.gameObject.GetComponent<Rigidbody>().velocity.y<=0)
                {
                    this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y-.05f,0);
                }
            }
            if (standCondition)
            {
                landed = true;
            }
                if (wallCondition)
            {
            lastWall = collision.gameObject;
            nearWall = true;
            movementLock = 0;
            if (collision.gameObject.transform.position.x <= this.gameObject.transform.position.x)
                canLeft = false;
            else if (collision.gameObject.transform.position.x >= this.gameObject.transform.position.x)
                canRight = false;
                
            }
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        string tag = collision.gameObject.tag;

        if(tag.Equals("Omniwall"))
        {
            //OmniWall
            if (collision.gameObject.Equals(lastGround))
            {
                landed = false;
                onJumpingGround = false;
                lastGround = null;
            }
            if (collision.gameObject.Equals(lastWall))
            {
                lastWall = null;
                nearWall = false;
                grabbedWall = false;
                canLeft = true;
                canRight = true;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag = other.gameObject.tag;

        if(tag.Equals("SKILL"))
        {
            Destroy(other.gameObject);
            string name =other.gameObject.GetComponent<Skill_learn>().ReturnName();
            switch (name)
            {
                case "MOVE":
                    gcs.SKILL_MOVE = true;
                    gcs.Dialog("You learned: Basic movement!;Press ← or → to move.");
                    break;
                case "JUMP":
                    gcs.SKILL_JUMP = true;
                    gcs.Dialog("You learned: Basic jump!;Press x to jump.");
                    break;
                case "DOUBLE_JUMP":
                    gcs.SKILL_DOUBLE_JUMP = true;
                    gcs.Dialog("You learned: Double Jump!;Press x while in the air to jump again.");
                    break;
                case "WALL_JUMP":
                    gcs.SKILL_WALL_JUMP = true;
                    gcs.Dialog("You learned: Wall Jump!;Press x when near a wall to jump again.;Move against a wall to grab it and slow your descent.;Grabbing walls resets your air jumps.");
                    break;
                case "DASH":
                    gcs.SKILL_DASH= true;
                    gcs.Dialog("You learned: Dash!;Press z while on the ground to move quickly for a short\namount of time.");
                    break;
                case "AIR_DASH":
                    gcs.SKILL_AIR_DASH = true;
                    gcs.Dialog("You learned: Air Dash!;Press z while in the air to move quickly for a short\namount of time.;Grabbing walls resets your air dashes.");
                    break;
            }
        }

        if (tag.Equals("Doorway"))
        {
            onDoorway = true;
            nextArea = other.gameObject.GetComponent<Doorway>().AreaName();
            doorNum = other.gameObject.GetComponent<Doorway>().DoorNum();
        }

        }
    private void OnTriggerStay(Collider other)
    {
        string tag = other.gameObject.tag;

        if (tag.Equals("Spikes"))
        {
            TakeDamage(5, tag);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        string tag = other.gameObject.tag;

        if (tag.Equals("Doorway"))
        {
            onDoorway = false ;
        }
    }

    private void Fire(string bulletName)
    {
        GameObject bullet = (GameObject)Instantiate(Resources.Load("Bullets/"+bulletName));
        bullet.GetComponent<Bullet_A>().Initialize(this.transform.position.x+(facingRight?this.transform.lossyScale.x/2:-this.transform.lossyScale.x / 2),this.transform.position.y+0.2f,10, 1,facingRight,5);
        fireTimeLeft = fireSpeed;
    }


    public void TakeDamage(int damage, string type)
    {
        
        if(invincibleTime<=0)
        {
            //Apply Damage.
            CurrentHealth -= damage;
            if(currentHealth<=0)
            {
                gcs.PlayerDeath();
            }

            if (grabbedWall)
            {
                grabbedWall = false;
                facingRight = !facingRight;
            }
            
            //Set invincibility
            invincibleTime = damageInvincibilityDuration;

            //Make the character flinch.
            flinchTimeLeft = flinchTime;
            
            //Cancel movement
            dashTimeLeft = 0;
        }
    }

    private void ReduceStatus()
    {

        float timePassed = Time.deltaTime;

        movementLock -= timePassed;
        wallGrabLock -= timePassed;
        invincibleTime -= timePassed;
        dashTimeLeft -= timePassed;
        flinchTimeLeft -= timePassed;
        fireTimeLeft -= timePassed;

        //Add status that ticks down to the list
        //float[] listToReduce = new float[] { movementLock, wallGrabLock, invincibleTime, dashTimeLeft, flinchTimeLeft, fireTimeLeft, jumpLock };

        //Time passed since last frame
        //float timePassed = Time.deltaTime;

        //for (int i = 0; i < listToReduce.Length; i++)
        //{
        //    if (listToReduce[i] >= -100)
        //        listToReduce[i] -= timePassed;
        //}
    }

    private void RequestStats()
    {
        this.moveSpeed=gcs.GetSpeed();
        this.jumpPower = gcs.GetJumpPower();
        this.airJumps = gcs.GetAirJumps();
        this.wallSlideSpeed = gcs.GetWallSlideSpeed();
        this.maxHealth = gcs.PlayerMaxHealth;
        this.CurrentHealth = maxHealth;
        this.dashSpeed = gcs.DashSpeed;
        this.dashTime = gcs.DashTime;
        this.flinchSpeed = gcs.FlinchSpeed;
        this.flinchTime = gcs.FlinchTime;
    }

    private bool CanJump()
    {
        return true;
        if (onJumpingGround && gcs.SKILL_JUMP)
        {
            return true;
        }
        return false;
    }
    private bool CanAirJump()
    {
        if (!onJumpingGround && !grabbedWall && airJumps>0 && gcs.SKILL_DOUBLE_JUMP)
        {
            return true;
        }
        return false;
    }
    private bool CanWallJump()
    {
        //If no wall has been contacted yet
        if (lastWall == null)
            return false;

        else if (nearWall && gcs.SKILL_WALL_JUMP)
            return true;

        return false;
    }
    private bool CanDash()
    {
        if (onJumpingGround && gcs.SKILL_DASH && flinchTimeLeft <= 0)
            return true;
        return false;
    }
    private bool CanAirDash()
    {
        if (!onJumpingGround && airDashes > 0 && gcs.SKILL_AIR_DASH && flinchTimeLeft <= 0)
        {
            return true;
        }
        return false;
    }
    private bool CanShoot()
    {
        if(fireTimeLeft <= 0 && dashTimeLeft <= 0 && flinchTimeLeft <=0 && !(weaponName.Equals("")))
        {
            return true;
        }
        return false;
    }

    private void ResetAirJumps()
    {
        this.airJumps = gcs.GetAirJumps();
    }
    private void ResetAirDashes()
    {
        this.airDashes = gcs.AirDashes;
    }

    public int CurrentHealth
    {
        get
        {
            return currentHealth;
        }

        set
        {
            currentHealth = value;
        }
    }

}
