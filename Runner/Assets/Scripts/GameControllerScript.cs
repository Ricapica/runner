﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameControllerScript : MonoBehaviour {

    //reference to this script
    public static GameControllerScript gcs;

    //Current Area
    Scene scene;
    string currentArea="";
    int entryDoor;
    

    //Player
    public GameObject player;

    //Player skills
    public bool SKILL_MOVE;
    public bool SKILL_JUMP;
    public bool SKILL_DOUBLE_JUMP;
    public bool SKILL_WALL_JUMP;
    public bool SKILL_DASH;
    public bool SKILL_AIR_DASH;

    //Player Stats
    private float speed = 5f;
    private float JumpPower = 13f;
    private int airJumps = 1;
    private int airDashes = 1;
    private float wallSlideSpeed = 3f;
    private float dashSpeed = 15f;
    private float dashTime = 0.25f;
    private float flinchSpeed = 5f;
    private float flinchTime = 0.2f;


    private int playerMaxHealth = 10;
    private float fireSpeed = 1f;
    private string weaponName = "";

    //Camera
    public GameObject mainCamera;
    int cameraZoom;
    

    //pause


    //Dialog
    bool displayText=false;
    string[] lines= new string[0];
    int currentChar = 0;
    int currentLine = 0;
    float setTimeBetweenChar = 0.01f;
    float lastCharTime = 0f;

    //GUI
    bool displayGUI = false;
    GUIStyle styleDialog = new GUIStyle();
    GUIStyle styleHealth = new GUIStyle();

    //Events
    bool startingTalk = false;


    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(player);
        DontDestroyOnLoad(mainCamera);
        
        gcs = this;
        //Load();
        Physics.gravity = new Vector3(0,-40,0);
    }

    private void Start()
    {
        mainCamera.GetComponent<Camera>().orthographicSize = 4;

        //Dialog style
        styleDialog.fontSize = 40;
        styleDialog.alignment = TextAnchor.UpperLeft;
        styleDialog.normal.textColor = Color.white;
        styleDialog.normal.background = MakeTex(1, 1, new Color(0.1f, 0.1f, 0.1f));

        //Health Style
        styleHealth.fontSize = 20;
        styleHealth.alignment = TextAnchor.UpperLeft;
        styleHealth.normal.textColor = Color.white;
        styleHealth.normal.background = MakeTex(1, 1, new Color(0f, 1f, 0f));
        
    }
    

    private void Update()
    {
        if (SceneManager.GetActiveScene().name.Equals("Starting") && !startingTalk)
        {
            Dialog("Welcome, helpless one.;Child you may be but infinite growth you possess.;Let falling teach you how to move on.;We now leave you in this land until you have learned\nenough.");
            startingTalk = true;
        }

        scene = SceneManager.GetActiveScene();
        if (!currentArea.Equals(scene.name))
        {
            //New scene loaded
            currentArea = scene.name;
            Vector3 position = new Vector3(0f, 0f, 0);
            if (currentArea.Equals("Main"))
            {
                displayGUI = true;
                if (entryDoor == 0)
                {
                    position = new Vector3(-20f, -20f, 0f);

                }
            }
            if (currentArea.Equals("Starting"))
            {
                displayGUI = true;
                if (entryDoor == 0)
                {
                    position = new Vector3(-7.5f, 22f, 0f);

                }
                if (entryDoor == 1)
                {
                    position = new Vector3(25f, -7f, 0f);

                }
            }
            if (currentArea.Equals("Starting-sub"))
            {
                displayGUI = true;
                if (entryDoor == 1)
                {
                    position = new Vector3(0f, -.5f, 0f);

                }
            }
            if (player != null)
            {
                player.transform.position = position;
                player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            }

        }
        if(player!=null)
            AdjustCameraPosition();

        Pause();
        if (displayText && Input.anyKeyDown)
        {
            if (currentChar == lines[currentLine].Length-1)
            {
                currentLine += 1;
                currentChar = 0;
            }
            else
            {
                currentChar=lines[currentLine].Length-1;
            }
        }

        if (currentLine < lines.Length)
            displayText = true;
        else
        {
            displayText = false;
            Unpause();
            currentLine = 0;
            lines = new string[0];
        }
    }

    public void EnterDoor(string area, int door)
    {
        entryDoor = door;
        SceneManager.LoadScene(area);
        
    }


    //     ↓CAMERA↓
    private void AdjustCameraPosition()
    {
        //make sure the camera is in perspective mode not orthographic or moving it on the z axis will make no difference
        Vector3 position=CalculateCameraPosition();
        mainCamera.transform.position=new Vector3(position.x, position.y, -10);
        
    }
    private Vector3 CalculateCameraPosition()
    {
        //The camera will track the player's position, trying to show what is important
        float playerX = player.transform.position.x;
        float playerY = player.transform.position.y;
        float cameraX=playerX;
        float cameraY=playerY;

        //Segment each area based on x axis, then y axis
        if (currentArea.Equals("Main"))
        {
            displayGUI = false;
            cameraX = 0;
            cameraY = 0;
        }

        if (currentArea.Equals("Starting"))
        {
            if(playerX<=-10)
            {
                cameraX = playerX;
                cameraY = 17;
            }
            else if(playerX<=-5)
            {
                if (cameraY > 17)
                {
                    cameraY = 17;
                }
            }
            else if(playerX<=10)
            {

            }
            else if(playerX<70)
            {
                if (cameraY <-6)
                {
                    cameraY = -6;
                }
            }
            else if (playerX < 83)
            {
                if (cameraY < -6)
                {
                    cameraY = -6;
                }
                cameraX = 77;
            }
        }

        if (currentArea.Equals("Starting-sub"))
        {
            if (playerX <= 5)
            {
                cameraX = playerX;
                if (cameraY > 7.5f)
                {
                    cameraY = 7.5f;
                }
            }
            else if (playerX <= 80)
            {
                if (cameraY < -17.5)
                {
                    cameraY = -17.5f;
                }
            }

        }

        Vector3 position = new Vector3(0.0f, 0.0f, 0.0f)
        {
            x = cameraX,
            y = cameraY
        };
        return position;
    }
    //     ↑CAMERA↑

    

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }



    private void OnGUI()
    {
        //displayGUI is disabled when player HP, etc. should not be shown
        if (displayGUI && player!=null)
        {
            //Health
            GUI.Box(new Rect(5, 5, (Screen.width / 4) * ((float)player.GetComponent<playerController>().CurrentHealth / playerMaxHealth), 20), " HP: " + player.GetComponent<playerController>().CurrentHealth + "/" + playerMaxHealth, styleHealth);

        }
        //Dialog
        if (displayText)
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height / 4), lines[currentLine].Substring(0, currentChar + 1), styleDialog);
            if ((currentChar < lines[currentLine].Length - 1) && Time.realtimeSinceStartup-lastCharTime>setTimeBetweenChar)
            {
                lastCharTime = Time.realtimeSinceStartup;
                currentChar += 1;
            }
        }

    }


    //stat modification methods
    public void SetSpeed(float s)
    {
        speed = s;
    }
    public float GetSpeed()
    {
        return speed;
    }

    public void SetJumpPower(float s)
    {
        JumpPower = s;
    }
    public float GetJumpPower()
    {
        return JumpPower;
    }

    public void SetAirJumps(int s)
    {
        airJumps = s;
    }
    public int GetAirJumps()
    {
        return airJumps;
    }

    public void SetWallSlideSpeed(float s)
    {
        wallSlideSpeed = s;
    }
    public float GetWallSlideSpeed()
    {
        return wallSlideSpeed;
    }

    public int PlayerMaxHealth
    {
        get
        {
            return playerMaxHealth;
        }

        set
        {
            playerMaxHealth = value;
        }
    }

    public float FireSpeed
    {
        get
        {
            return fireSpeed;
        }

        set
        {
            fireSpeed = value;
        }
    }

    public string WeaponName
    {
        get
        {
            return weaponName;
        }

        set
        {
            weaponName = value;
        }
    }

    public float DashSpeed
    {
        get
        {
            return dashSpeed;
        }

        set
        {
            dashSpeed = value;
        }
    }

    public float DashTime
    {
        get
        {
            return dashTime;
        }

        set
        {
            dashTime = value;
        }
    }

    public int AirDashes
    {
        get
        {
            return airDashes;
        }

        set
        {
            airDashes = value;
        }
    }

    public float FlinchSpeed
    {
        get
        {
            return flinchSpeed;
        }

        set
        {
            flinchSpeed = value;
        }
    }

    public float FlinchTime
    {
        get
        {
            return flinchTime;
        }

        set
        {
            flinchTime = value;
        }
    }

    public void Dialog(string info)
    {
        lines = info.Split(';');
    }


    public void Pause()
    {
        Time.timeScale = 0f;
    }
    public void Unpause()
    {
        Time.timeScale = 1f;
    }


    //Player Death

    public void PlayerDeath()
    {
        Destroy(player.GetComponent<MeshRenderer>());
    }




    //Save data


    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        Playerdata data = new Playerdata
        {
            SKILL_MOVE = SKILL_MOVE,
            SKILL_JUMP = SKILL_JUMP,
            SKILL_DOUBLE_JUMP = SKILL_DOUBLE_JUMP,
            SKILL_WALL_JUMP = SKILL_WALL_JUMP,
            SKILL_DASH = SKILL_DASH,
            SKILL_AIR_DASH = SKILL_AIR_DASH
        };
        bf.Serialize(file, data);
        file.Close();
    }
    public void Load()
    {
        if(File.Exists(Application.persistentDataPath+"/playerInfo.dat"))
        {

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            Playerdata data = (Playerdata)bf.Deserialize(file);
            try
            {
                SKILL_MOVE = data.SKILL_MOVE;
                SKILL_JUMP = data.SKILL_JUMP;
                SKILL_DOUBLE_JUMP = data.SKILL_DOUBLE_JUMP;
                SKILL_WALL_JUMP = data.SKILL_WALL_JUMP;
                SKILL_DASH = data.SKILL_DASH;
                SKILL_AIR_DASH = data.SKILL_AIR_DASH;
            }
            catch(Exception)
            {

            }
            file.Close();
            //Save();
        }
        else
        {
            print("Loading: ");
            //initialize first run variables
            SKILL_MOVE = false;
            SKILL_JUMP = false;
            SKILL_DOUBLE_JUMP = false;
            SKILL_WALL_JUMP = false;
            SKILL_DASH = false;
            SKILL_AIR_DASH = false;
        }
    }
}



[Serializable]
class Playerdata
{
    //Player Skills
    public bool SKILL_MOVE=false;
    public bool SKILL_JUMP=false;
    public bool SKILL_DOUBLE_JUMP=false;
    public bool SKILL_WALL_JUMP=false;
    public bool SKILL_DASH = false;
    public bool SKILL_AIR_DASH = false;
    public int XPresses = 0;
}

