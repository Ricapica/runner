﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doorway : MonoBehaviour {

    public string doorTo;   //which area this door leads to
    public int doorNum;     //which door in the next area this leads to

	// Use this for initialization
	void Start () {
		
	}
    public string AreaName()
    {
        return doorTo;
    }
    public int DoorNum()
    {
        return doorNum;
    }
}
