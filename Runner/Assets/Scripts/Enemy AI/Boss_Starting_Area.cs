﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Starting_Area : MonoBehaviour {

    //Game Controller
    GameControllerScript gcs;
    bool activated = false;
    //Starting location
    float spawnX;
    float spawnY;

    //Current location at any frame.
    float currentX;
    float currentY;

    //Player location
    float playerX;
    float playerY;

    //Stats
    public int health = 15;


    /*AI Description
     * 
     * This Boss will Chase the player around in its first phase only along the ground
     * Chasing is done in small bursts of motion.
     * Once a charge is started, it does not change direction.
     * During the second phase, the boss will perform screenwide faster charges and slam into walls.
     * 
    */
    //AI Variables
    float chaseTime = 0.6f;
    float chaseTimer = 0;
    float idleTime = 2;
    float idleTimer = 2;

    float chaseSpeed = 12f;              //Speed of the charge.
    bool chargeSide;                    //False for left, true for right.
    //Visuals
    bool flash;
    Material material;



    void Start()
    {
        gcs = GameControllerScript.gcs;


        //Remember starting position.
        SpawnX = this.gameObject.transform.position.x;
        SpawnY = this.gameObject.transform.position.y;

        //Visuals
        flash = false;
        material = GetComponent<Renderer>().material;

        Physics.IgnoreCollision(gcs.player.GetComponent<Collider>(), GetComponent<Collider>());
        this.gameObject.SetActive(false);
    }


    void Update()
    {
        
        //tickdown timers
        chaseTimer -= Time.deltaTime;
        idleTimer -= Time.deltaTime;

        //Be aware of current position.
        CurrentX = this.gameObject.transform.position.x;
        CurrentY = this.gameObject.transform.position.y;

        //Be aware of player position.
        playerX = gcs.player.transform.position.x;
        playerY = gcs.player.transform.position.y;

        //AI
        if (activated)
        {
            if (idleTimer <= 0 && chaseTimer <= 0)
            {
                chargeSide = FindChaseDirection();
                chaseTimer = chaseTime;
                idleTimer = chaseTime + idleTime;
            }
            if (chaseTimer > 0)
            {
                GroundChase();
            }
        }


        //Visuals
        if (flash)
        {
            flash = false;
            //Flicker white
            material.SetColor("_EmissionColor", Color.white);
        }
        else
        {
            material.SetColor("_EmissionColor", Color.black);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag = other.gameObject.tag;
        
        if (tag.Equals("Bullet_Player"))
        {

            int damageTaken = other.GetComponent<Bullet_A>().Damage;
            health -= damageTaken;
            flash = true;
            Destroy(other.gameObject);
            if (health <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        string tag = other.gameObject.tag;


        if (tag.Equals("Player"))
        {
            if (chaseTimer > 0)
            {
                other.gameObject.GetComponent<playerController>().TakeDamage(3, "charge");
            }
            else
            {
                other.gameObject.GetComponent<playerController>().TakeDamage(1, "contact");
            }
        }
    }

    private bool FindChaseDirection()
    {
        if (currentX <= playerX)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void GroundChase()
    {
        //if (Mathf.Abs(currentX - playerX) <= 5 && Mathf.Abs(currentX - playerX) >= 1)
        this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(chaseSpeed*(chargeSide?1:-1), 0, 0);
    }
    
    public void Activate()
    {
        activated = true;
    }

    public float SpawnX
    {
        get
        {
            return spawnX;
        }

        set
        {
            spawnX = value;
        }
    }

    public float SpawnY
    {
        get
        {
            return spawnY;
        }

        set
        {
            spawnY = value;
        }
    }

    public float CurrentX
    {
        get
        {
            return currentX;
        }

        set
        {
            currentX = value;
        }
    }

    public float CurrentY
    {
        get
        {
            return currentY;
        }

        set
        {
            currentY = value;
        }
    }
}
