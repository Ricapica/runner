﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Training_Dummy : MonoBehaviour {

    //Game Controller
    GameControllerScript gcs;

    //Starting location
    float spawnX;
    float spawnY;

    //Current location at any frame.
    float currentX;
    float currentY;

    //Stats
    public int health = 15;

    //Visuals
    bool flash;
    Material material;

    

    void Start () {
        gcs = GameControllerScript.gcs;


        //Remember starting position.
        SpawnX = this.gameObject.transform.position.x;
        SpawnY = this.gameObject.transform.position.y;

        //Visuals
        flash = false;
        material=GetComponent<Renderer>().material;

        Physics.IgnoreCollision(gcs.player.GetComponent<Collider>(), GetComponent<Collider>());
    }
	

	void Update () {
        //Be aware of current position.
		CurrentX = this.gameObject.transform.position.x;
        CurrentY = this.gameObject.transform.position.y;


        //Visuals
        if(flash)
        {
            flash = false;
            //Flicker white
            material.SetColor("_EmissionColor", Color.white);
        }
        else
        {
            material.SetColor("_EmissionColor", Color.black);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag = other.gameObject.tag;

        if(tag.Equals("Bullet_Player"))
        {
            
            int damageTaken = other.GetComponent<Bullet_A>().Damage;
            health -= damageTaken;
            flash = true;
            Destroy(other.gameObject);
            if (health<=0)
            {
                Destroy(this.gameObject);
            }
        }
        

    }
    private void OnTriggerStay(Collider other)
    {
        string tag = other.gameObject.tag;

        if (tag.Equals("Player"))
        {
            other.gameObject.GetComponent<playerController>().TakeDamage(1, "contact");
        }
    }


    public float SpawnX
    {
        get
        {
            return spawnX;
        }

        set
        {
            spawnX = value;
        }
    }

    public float SpawnY
    {
        get
        {
            return spawnY;
        }

        set
        {
            spawnY = value;
        }
    }

    public float CurrentX
    {
        get
        {
            return currentX;
        }

        set
        {
            currentX = value;
        }
    }

    public float CurrentY
    {
        get
        {
            return currentY;
        }

        set
        {
            currentY = value;
        }
    }
}
