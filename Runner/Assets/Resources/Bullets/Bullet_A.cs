﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_A : MonoBehaviour {

    float moveSpeed = 0f;
    float lifeTime = 0f;
    bool facingRight = false;

    int damage = 0;

    

    public void Initialize(float startX,float startY, float moveSpeed, float lifeTime, bool facingRight, int damage)
    {
        this.moveSpeed = moveSpeed;
        this.lifeTime = lifeTime;
        this.facingRight = facingRight;
        this.gameObject.transform.position=new Vector3(startX, startY, this.gameObject.transform.position.z);
        this.Damage = damage;
    }

    void Update () {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(this.gameObject);
        this.GetComponent<Rigidbody>().velocity = new Vector3(facingRight?moveSpeed:-moveSpeed,0,0);
	}



    public int Damage
    {
        get
        {
            return damage;
        }

        set
        {
            damage = value;
        }
    }
}
